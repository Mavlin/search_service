import fetchJsonp from 'fetch-jsonp';

const v = '5.69',
    access_token = 'b2328a35750819565b4d936f471027ffd10f7198088c4ef9bf3423d11c4257d79f357641ff160e3492e39',
    url = 'https://api.vk.com/method/'


export function getData( a, method, count, offset=0 ) {
    let URL = url, arg;
    switch (method){
        case 'users.search':
            arg = {
                q: '"' + a + '"',
                fields: "photo_200",
                offset: offset,
                sort: 1,
                count: count || 10
            };
            break;
        case 'users.get':
            arg = {
                user_ids: a,
                fields: "photo_200, sex, bdate, city, country, home_town"
                // " photo_id, has_photo, verified," +
                // "photo_50, photo_100, photo_200_orig, photo_200, photo_400_orig, photo_max, " +
                // "photo_max_orig, online, domain, has_mobile, contacts, site, education, universities," +
                // "schools, status, last_seen, followers_count, common_count, occupation, nickname," +
                // "relatives, relation, personal, connections, exports, wall_comments, activities, " +
                // "interests, music, movies, tv, books, games, about, quotes, can_post, can_see_all_posts," +
                // " can_see_audio, can_write_private_message, can_send_friend_request, is_favorite, " +
                // "is_hidden_from_feed, timezone, screen_name, maiden_name, crop_photo, is_friend, " +
                // "friend_status, career, military, blacklisted, blacklisted_by_me"
            };
            break;
    }
    arg.v = v
    arg.access_token = access_token + ''
    let metod = method + '?',
        req = URL + metod, k = 0,
        argCnt = Object.keys(arg).length
    for (let i in arg) {
        k++
        req += i + '=' + arg[i] + (k < argCnt ? '&' : '')
    }
    console.log(req)
    // throw new Error ( 'some err' )
    return fetchJsonp(req)
}
// helper
export function Run(generator, yieldValue) {
    // debugger
    let next = generator.next(yieldValue);
    if (!next.done) {
        next.value.then(
            // console.log(yieldValue)
            result => {
                // console.log(result)
                Run(generator, result)
            },
            err => {
                console.log(err)
                generator.throw(err)
            }
        );
    } else {
        // console.log(next.value);
    }

}
