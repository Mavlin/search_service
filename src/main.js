import './css/fonts-original.scss'
import './css/style.scss';

import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)

let router = new VueRouter({
    routes:[
        {path: '/', name: 'home', component: App},
        {path: '/user_profile/:id', name: 'user_profile', component: App},
    ]
})
new Vue({
    router: router,
}).$mount('#app')

/*
* https://github.com/epicmaxco/vuestic-admin
* https://github.com/egoist/vuepack
* */























// var arr = [];
//
// for (var i = 2; i <= 100; i++) {
//     arr.push(i)
// }
//
// console.log(arr)
function removePrimes(arr) {

    // получение максимального элемента массива
    function getMaxValue(array){
        var max = array[0]; // берем первый элемент массива
        for (var i = 0; i < array.length; i++) { // переберем весь массив
            // если элемент больше, чем в переменной, то присваиваем его значение переменной
            if (max < array[i]) max = array[i];
        }
        // возвращаем максимальное значение
        return max;
    }
    // получение всех простых чисел до найденного предела
    function getPrimes(max) {
        var sieve = [], i, j, primes = [];
        for (i = 2; i <= max; ++i) {
            if (!sieve[i]) {
                // i has not been marked -- it is prime
                primes.push(i);
                for (j = i << 1; j <= max; j += i) {
                    sieve[j] = true;
                }
            }
        }
        return primes;
    }
    // вычитание полученного массива из исходного
    function difference(a1, a2) {
        var result = [];
        for (var i = 0; i < a1.length; i++) {
            if (a2.indexOf(a1[i]) === -1) {
                result.push(a1[i]);
            }
        }
        return result;
    }

    let maxEl = getMaxValue(arr)
    let primesInEl = getPrimes(maxEl)
    return difference(arr, primesInEl)

}



// console.log(getPrimes(100))
// console.log(maxEl)
// console.log(primesInEl)
// console.log(diff)






// const comp = {
//     template: '<div class="container">\n' +
//     '  <header>\n' +
//     '    <slot name="header">Мой заголовок</slot>\n' +
//     '  </header>\n' +
//     '  <main>\n' +
//     '    <slot></slot>\n' +
//     '  </main>\n' +
//     '  <footer>\n' +
//     '    <slot name="footer"></slot>\n' +
//     '  </footer>\n' +
//     '</div>'
// }

// let Parent = new Vue({
//     el: '#test',
//     template: '<app-layout>\n' +
//     '  <h1 slot="header">Здесь мог бы быть заголовок страницы</h1>\n' +
//     '\n' +
//     '  <p>Абзац основного контента.</p>\n' +
//     '  <p>И ещё один.</p>\n' +
//     '\n' +
//     '  <p slot="footer">Вот контактная информация</p>\n' +
//     '</app-layout>',
//     components:{
//         'app-layout': comp
//     }
// })


// var bus = new Vue()

// new Vue({
//     el: '#app',
//     router: router,
//     // template: '<App/>',
//     // components:{
//     //     App
//     // }
// })
// 0. При использовании модульной системы (напр. vue-cli),
// импортируйте Vue и VueRouter и затем вызовите `Vue.use(VueRouter)`

// 1. Определение используемых компонентов
// Они могут быть импортированы из внешних файлов
// const Foo = { template: '<div>foo</div>' }
// const Bar = { template: '<div>bar</div>' }

// 2. Определение путей
// Каждый путь должен указывать на компонент
// "Компонентом" может быть как созданный через `Vue.extend()`
// полноценный конструктор, так и просто объект с настройками компонента
// Вложенные пути будут рассмотрены далее.
// const routes = [
//     { path: '/foo', component: Foo },
//     { path: '/bar', component: Bar }
// ]

// 3. Создаём экземпляр роутера с опцией `routes`
// Можно передать и другие опции, но пока не будем усложнять
// const router = new VueRouter({
//     routes // сокращение от `routes: routes`
// })

// 4. Создаём и монтируем корневой экземпляр Vue нашего приложения.
// Удостоверьтесь, что передали экземпляр роутера в опции `router`,
// что позволит приложению знать о его наличии
// new Vue({
//     router
// }).$mount('#app')

// Всё, приложение работает! ;)




// console.log('inn')
// Vue.config.productionTip = false
/* eslint-disable no-new */
/*

*/

