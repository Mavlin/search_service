# vue-6-single-file-components

> A Vue.js project

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

Гарантирована работа в Google Chrome 64 версии, hot reload проверен в OS Windows 8.
совсем не работает в IE и не запоминает положение в списке пользователей в FireFox
Opera немного меняет верстку.
